import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class MyHashSet<T> implements Set<T> {
    private static final int DEFAULT_CAPACITY = 16;

    private static class Node{
        Object value;
        Node next;
    }
    private Node[] nodes;
    private int size = 0;

    public MyHashSet(int capacity) {
        this.nodes = new Node[capacity];
    }

    public MyHashSet() {
        this(DEFAULT_CAPACITY);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public boolean contains(Object o) {
        int index = getIndex(o);
        for (Node cur = nodes[index]; cur!=null; cur=cur.next){
            if(cur.value.equals(o)){
                return true;
            }
        }
        return false;
    }

    private int getIndex(Object o) {
        return Math.abs(o.hashCode()%nodes.length);
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }
}
